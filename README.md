# Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap

+ Conjuntos, Aplicaciones y funciones (2002)
++ Una de las funciones de las matematicas es:
+++ Reflexxionar y razonar.
++ ¿A que se le denomina conjunto?
+++ Son una coleccion de elementos 
+++ Son ideas intuitivas 
+++ Su relacion de pertenencia es la que las diferencia. 
++ ¿Que es inclucion de conjuntos?
+++ Es la nocion que hay entre conjuntos es decir \nla que todos los elementos del primero \nson elementos del segundo conjunto.
++ Existen 2 tipos de conjuntos 
+++ Universal :es un conjunto de referencias 
+++ Vacio: Es el conjunto que no tiene elementos. \n-Necesidad de cerrar bien las cosas. 
++ Las representaciones graficas ayudan a 
+++ Comprender las posiciones 
++ Los conjuntos que no tienen elemntos en la representacion \nde los conjuntos suelen representarse mendiente \ndiagramas o dibujos, \nestos estan denominados por Diagramas de Venn.
++ ¿Que es el cardenal de un conjunto? 
+++ Es el numero de elementos que conforman un conjunto.
++ Cardinal de un cojunto
++ Elementos que conforman un conjunto 
+++ Propiedades:
++++ Formula para resover: \n-Union de conjuntos: la cardinalidad de una union es igual al cardinal de uno de los conjuntos, \nel conjunto "A" mas el cardinal del \nsegundo conjunto "B" menos el cardinal de la interseccion. 
++++ Acotacion de cardinales.
+++++ Mayor 
+++++ Menor 
+++++ Igual.  
++ Aplicaciones y funciones.
+++ Se denomina aplicaciones o funcion 
+++ Es una transformacion que convierte a cada uno de los \nelementos del conjunto en unico elemento de un conjunto final.
++++ Tipos 
+++++ Inyectiva 
+++++ Sobreyectiva
+++++ Biyectiva
++++ Funciones 
+++++ Grafica de la funcion 
+++++ Serie de puntos 
+++++ Parabolas 
+++++ Linea recta.
@endtmindmap
```
# Funciones (2010)
```plantuml
@startmindmap

+ Funciones (2010)
++ Funciones surgidas de la necesidad de comprender e interpretar \nel entorno asi como de resolver pproblemas diarios mediante metodos cada vez mas complejos acorde a sus problemanticas \nlas funciones surgen de.
+++ observacion 
+++ Experimentacion 
+++ En casos espesificos se obtendran diversos resultados basados  \nen una variable definida por ejemplo.
++++ La temperatura en funcion de la hora, es decir: \nCambios en los resultados en base a esa variable 
++ Las funciones se pueden interpretar en como influye un grupo de numeros en otro grupo de numeros.
++ Las funciones se pueden representar en el plano de manera grafica o carteciana. 
++ asignando los ejes
++ los valores de los conjuntos y de las imagenes 
++ Laas funciones se clasifican observando su comportamiento en \nla representacion grafica.
+++ Crecientes 
+++ Decrecientes 
++ Limite 
+++ El limite se puede definir como cuando los valores de la variable \nestan cerca de un punto x, \n los valores de sus imagenes estan cerca de un determinado valor. 
+++ Tambien se pueden ir acercando a los valores cuando existe un "salto" en la funcion, \nes decir cuando no existe los valores reales de la imagen en cierto valor \nde la variable.
+++ Dependiendo si la funcion es continua o no se podran identificar el o los limites de la misma.
++++ Continuo  
++++ Discintinuo
++ Derivada 
+++ La derivada surge por la complejidad de analisis de las funciones que se presentan en la vida real 
+++ Se resume como el intento de aproximar una funcion mas complicada con una funcion mas simple 
+++ Una derivada busca reflejar una funcion mas compleja de una manera mas \nsimple en punto en espesifico \nmediante una funcion lineal simple.
+++ Existen "atajos" o formulas \npara no tener que formular todo el concepto de derivada para funciones mas simples. \n por ejemplo la derivada de x^y es igual a y•x^(y-1).
++ Intervalo
+++ Rango de los valores que puede ocupar.
++ Dominio.
+++ Un conjunto de elementos "x" de la variable independiante. 
@endtmindmap
```
# La matemática del computador (2002)
```plantuml
@startmindmap

+ La matemática del computador (2002)
++ Aritmetica del computador. (Tambien conocido como \naritmetica con precision finita)
+++ Surge por la incapacidad de responder cifras, infinitas \nen un numero finito de especios o pocisiones de memoria del computador 
+++ Es la manera de computar los calculos meante conceptos que limitan los calculos,\na una aritmetica finita.
++++ Error o dijitos de error.
++++ Numero aproximado.
++++ Digitos significativos 
+++++ Se toman los digitos que represnetan correctamente el valor util 
++++ Truncar y redondear un numero.
+++++ Truncar es cortar un numero en cierta cantidad de cifras para poder representarlo en dada cantidad de posiciones o espacios.
+++++ El redondear comppletementa el truncar para reducir al minimo los errores por el truncamiento de un numero.
++ Sistemas de numeracion.
+++ Binario. 
++++ Basado en la interpretacion de la logica booleana \ny/o en la pertencia de conjuntos 
++++ Basado en dos unicades basicas 0 y 1 
++++ Crea o se representa como todo lo grafico, \nvideo o fotos, que vemos hoy en dia, \nmediante conjutos y grupos de 0´s y 1´s.
++++ El computador puede crear estructuras complejas basado en el binario. 
++++ Realiza la materializacion de un numero real entero mendiante 3 metos / conceptos 
+++++ Representacion en magnitud signo 
+++++ Representacion en exeso.
+++++ Representacion en complemento dos.
++++ Se maneja o funciona al convertir posiciones de memoria \nen binario a un numero real entero.
++++ Asi mismo se manejan los conceptos en los que traajan varios computadores y calculadoras cientificas 
+++++ Punto flotante 
+++++ Punto 12, 12.7, 12.5
+++++ Desbordamiento 
++ Octal 
+++ Sistema a base de ocho 
++ Hexadecimal.
+++  Ambos sistemas pueden representar numero de manera compacta.
@endtmindmap
```


Materia: Programacioón Lógica y Funcional.

Elaborado por: Martha Valeria Ramirez Garcia. 

Numero de control: 17161209
